$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	// CARROSSEL DE DESTAQUE
	$(document).ready(function() {
		setTimeout(function(){
			var video = $(".video0");
			video[0].load();
			video[0].play();	
		}, 1000)
		$("#carrosselDestaqueTopo").owlCarousel({
			items : 1,
	        dots: true,
	        loop: false,
	        lazyLoad: true,
	        mouseDrag:true,
	        touchDrag  : true,	       
		    autoplayTimeout:5000,
		    autoplayHoverPause:true,
		    scroll:true,
		    navigation: true,
		    smartSpeed: 450,
	   		    
		    
		});
	});


	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 6,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,
        autoplay:true,	       
	    autoplayTimeout:2000,
	    autoplayHoverPause:true,
	    scroll:true,
	    navigation: true,
	    smartSpeed: 450,

	    // CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:2
            },
            600:{
                items:4
            },
           
            991:{
                items:4
            },
            1024:{
                items:6
            },
            1440:{
                items:6
            },
            			            
        }		    		   		    
	    
	});

	//CARROSSEL DE DESTAQUE
	$("#carrosselDepoimentos").owlCarousel({
		items : 1,
        dots: true,
        loop: true,
        autoplay:true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:3000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});
		
		//CARROSSEL DE DESTAQUE
	$("#carrosselParceria").owlCarousel({
		items : 3,
        dots: false,
        loop: true,
        autoplay:true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:3000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1,
                loop:true,
            },
            600:{
                items:2,
                loop:true,
            },
           
            991:{
                items:2
            },
            1024:{
                items:3	
            },
            1440:{
                items:3
            },
            			            
        }	
	});
			var carrossel_parceria = $("#carrosselParceria").data('owlCarousel');
			$('#flechaEsquerda').click(function(){ carrossel_parceria.prev(); });
			$('#flechaDireita').click(function(){ carrossel_parceria.next(); });

		// TEXTO CORRENDO NA TELA
		function typeWritter(texto,idElemento,tempo){
	    var char = texto.split('').reverse();
	    var typer = setInterval(function () {
		        if (!char.length) return clearInterval(typer);
		        var next = char.pop();
		        document.getElementById(idElemento).innerHTML += next;
		    }, tempo);
		}

	// typeWritter('Linda, Simpatica, pontual e arrumada.','text',45.00);

	 
     $.fn.isOnScreen = function(){
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};

		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();
		
		var bounds = this.offset();
	    bounds.right = bounds.left + this.outerWidth();
	    bounds.bottom = bounds.top + this.outerHeight();
		
	    return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};

        var text1 = false;
        var text2 = false;
        var text3 = false;
        var text4 = false;
        var text5 = false;
    $(window).scroll(function(){
		var larguraWidth = $(window).width();
		if ($('#texto').isOnScreen() == true && text1 == false) {
	  		
			setTimeout(function(){ 

		   		typeWritter('Linda, Simpatica, pontual e arrumada.','texto',45.00);
		   		
		    }, 1000);
		  
			text1 = true;
		};

		if ($('#text2').isOnScreen() == true && text2 == false) {
	  		
				setTimeout(function(){ 
			   		typeWritter('Linda, Simpatica, pontual e arrumada.','text2',45.00);
			    }, 2000);
			  
				text2 = true;
		};

		
		if ($('.verificacaoVisibilidade1').isOnScreen() == true && text3 == false) {
			
				$(".verificacaoVisibilidade1").addClass("filtroPretoEBranco");
				$(".verificacaoVisibilidade1").removeClass("filtroPreto");

				text3 = true;
			
		};

		
			if ($('.verificacaoVisibilidade2').isOnScreen() == true && text4 == false) {
			
					$(".verificacaoVisibilidade2").addClass("filtroPretoEBranco");
					$(".verificacaoVisibilidade2").removeClass("filtroPreto");

					text4 = true;
				
			};
		

		
			if ($('.verificacaoVisibilidade3').isOnScreen() == true && text4 == false) {
				
					$(".verificacaoVisibilidade3").addClass("filtroPretoEBranco");
					$(".verificacaoVisibilidade3").removeClass("filtroPreto");
					
					text5 = true;
				
			};
		
	});		


	//PREVENT DEFAULT CLICK DO `a`
	$( ".pg-inicial .carrosselDestaque .item" ).click(function(e) {
  		event.preventDefault();
	 	let titulo = $(this).children('a').attr("data-titulo");
	 	let descricao = $(this).children('a').attr("data-descricao");
	 	let dataUrlImg = $(this).children('a').attr("data-urlImg");
	 	$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeIn();
	 	$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos h2").text(titulo);
	 	$(".pg-inicial .carrosselDestaque .imagemCentralizada").css({"background":"url("+dataUrlImg+")"});

	 	$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeOut();
			}
		});


		$( ".fecharModalDetalhesPortfolio" ).click(function(e) {
			$(".pg-inicial .carrosselDestaque .detalhesCarrosselServicos").fadeOut();
		});
	 	
	 	

 	});
   	
   	$("a#fancy").fancybox({
		'titleShow' : false,
		openEffect	: 'elastic',
		closeEffect	: 'elastic',
		closeBtn    : true,
		arrows      : true,
		nextClick   : true
	});
		
	$( ".pg-inicial .carrosselDestaque .detalhesCarrosselServicos .areaTexto .abrirModalGaleria" ).click(function(e) {
  		event.preventDefault();
  	});


	$( ".pg-inicial .carrosselDestaque .detalhesCarrosselServicos .areaTexto .abrirModalGaleria" ).click(function(e){
		$('.pg-galeria').fadeIn();
		$("body").addClass("travarScroll");

	});


	$( ".voltarPaginaAnterior" ).click(function(e) {
  		event.preventDefault();
  	});

	$( ".voltarPaginaAnterior" ).click(function(e) {
	 	$('.pg-galeria').fadeOut();
	 	$("body").removeClass("travarScroll");
	});

	// FUNCAO PARA PEGAR O TAMANHO DA TELA
	$('#carrosselDestaqueTopo .item').css({'height':($(window).height())+'px'});
		$(window).resize(function(){
			$('#carrosselDestaqueTopo .item').css({'height':($(window).height())+'px'});
		});

	//SCRIPT APARECER BOTAO VOLTAR AO TOPO
  	$(window).bind('scroll', function () {
       
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 400) {
            $(".backToTop").fadeIn();
       }else{
            $(".backToTop").fadeOut();

       }
    });


  	//SCRIPT VOLTAR AO TOPO
  	$(document).ready(function() {
	$('#subir').click(function(){
		$('html, body').animate({scrollTop:0}, 'fast');
		
		});
	});

	//SCRIPT ABRIR FORMULÁRIO DE PARCERIA
	$( ".abrirFormularioParceria" ).click(function() {
  		$(".pg-parceria").fadeIn();
  		$("body").addClass('travarScroll');
  	});

	$( ".fecharModalFormularioParceria" ).click(function() {
  		$(".pg-parceria").fadeOut();
  		$("body").removeClass('travarScroll');
  	});

	 // SCRIPT PARA MOSTRAR O MENU FIXO APÓS O SCROLL
    $(window).bind('scroll', function () {
		var alturaDaTela;
		alturaDaTela = $(window).height();
       	var alturaScroll = $(window).scrollTop();
	     
       	if (alturaScroll > alturaDaTela) {
       	 	$(".topo").fadeIn('fast');
       	}else{
     		$(".topo").fadeOut('fast');
       	}
    });     


	// if (alturaDaTela <= 480) { 
	// 	$(window).resize(function(){
	// 			$('.formularioParceria').css('padding-top: 0');
	// 	});
	// }
		
	// ANIMAÇÃO DAS ANCORAS
	var $doc = $('html, body');
	$('a.scrollTop').click(function() {
		$(".navbar-collapse").removeClass('in');
	    $doc.animate({
	        scrollTop: $( $.attr(this, 'href') ).offset().top
	    }, 1000);

	    return false;
	});

	$( ".abrirModalPaceiro .abrirFormularioParceria" ).click(function(e) {
			event.preventDefault();
     });

});
